# README

A real simple Nix example of building a Docker Image. 


To build the image:

```bash
nix build '.#dockerImage'
```

To load the image into Docker:

```bash
docker load < result
```

Run the Image:

```bash
docker run -it --rm hello-julia
```
