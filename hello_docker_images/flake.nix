{
  description = "A simple Docker image";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

  outputs = { self, nixpkgs }: let
    system = "x86_64-linux";
    pkgs = nixpkgs.legacyPackages.${system};
    helloWorldScript = pkgs.runCommandNoCC "hello-world-script" {
      src = "${self}/hello_julia.jl";
      dst = "hello_julia.jl";
    } ''
      mkdir -p $out/bin
      cp $src $out/bin/$dst
      '';
  in {

    packages.${system}.dockerImage = pkgs.dockerTools.buildLayeredImage {
      name = "hello-julia";
      tag = "latest";
      config = {
        Cmd = [ "${pkgs.julia}/bin/julia" "/bin/hello_julia.jl" ];
      };
      contents = [ pkgs.julia helloWorldScript ];
    };

    devShell.${system} = pkgs.mkShell {
      buildInputs = [ pkgs.docker ];
    };
  };
}

