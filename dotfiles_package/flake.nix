{
  description = "A very basic flake";

  inputs = {
    nixpkgs = {
      url = github:nixos/nixpkgs?ref=nixos-22.11;
    };

  };
  outputs = { 
    self, 
    nixpkgs 
  }: let
    system = "x86_64-linux";
    pkgs = import nixpkgs { inherit system;};
  in {
    packages.${system} = {
      # learned you an have multiple packages in a flake.. 
      myPackage = pkgs.callPackage ./. {};
      # this makes it the above the default package in the event there were more
      default = self.packages.${system}.myPackage;
    };
  };
}
