{
  stdenv,
  julia,
  fetchgit,
  rsync,
  ...
}: stdenv.mkDerivation {
  pname = "myPackageName";
  version = "v0.0.1";

  # nix-prefetch-git <repo>
  src = fetchgit {
    url = "https://gitlab.com/usmcamp0811/dotfiles.git";
    rev = "a70cb28c667a54a5682e289dc5fba1aa2846dad1";
    sha256 = "12vifiwwydvgxjqjgcnyiapdxnvddpfvi68ld0bzfzh2nxv3bi3q";
    };

  buildInputs = [julia rsync];

  # buildPhase = ''
  #   echo "do some build things"
  #   julia src/hello.jl
  # '';

  # this is runtime dependencies.. so bash would be availble at run time
  # nativeBuildInputs = [
  #   zsh
  # ]

  installPhase = ''
    mkdir -p $out/
    rsync --recursive --verbose --exclude '.git' .* $out/
    rsync --recursive --verbose --exclude '.git' * $out/
  '';
}
  
