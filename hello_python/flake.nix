{ pkgs ? import <nixpkgs> {} }:

let
  image = pkgs.dockerTools.buildLayeredImage {
    name = "my-nix-image";
    tag = "latest";

    contents = [ pkgs.python3 pkgs.gcc ];

    config = {
      Cmd = [ "python3" ];
    };
  };
in
  image
